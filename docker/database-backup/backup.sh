#!/bin/bash

set -e

now=$(date --utc +"%Y-%m-%dT%TZ")

databases=(
    alerts
    audit
    authorization
    calendars
    devices
    docs
    dx
    emails
    facebook
    features
    files
    fonts
    google
    hub
    # hydra
    layouts
    licensing
    linkedin
    login
    mailchimp
    media
    microsoft
    notifications
    organizations
    partners
    payment
    playlists
    policies
    powerbi
    registration
    scim
    sharepoint
    slack
    slides
    templates
    users
    wayke
    webmegler
)

/google-cloud-sdk/bin/gcloud auth activate-service-account --key-file ${GOOGLE_APPLICATION_CREDENTIALS}

for database in ${databases[@]}; do
    cockroach dump --insecure ${database} --url ${DATABASE_URL} > /tmp/${database}.sql
    /google-cloud-sdk/bin/gsutil cp -Z /tmp/${database}.sql gs://${GCLOUD_BUCKET_NAME}/${now}/${database}.sql
    rm /tmp/${database}.sql
done
